/*
	A simple helper to set flags based on struct tags

	to tag a struct field use the following syntax:
	`flag:"name, usage"`
	everything behind the first comma is considered the usage
	to set a default value, set the field to the appropriate value
*/
package flagutil

import (
	"flag"
	"reflect"
	"strings"
	"time"
)

// SetFromFlagSet sets the flags of the struct on the given flagset
// if a field is not of any supported type, SetFromFlagSet will panic
func SetFromFlagSet(f *flag.FlagSet, target interface{}) {
	val := reflect.ValueOf(target)
	if val.Kind() != reflect.Ptr || val.Type().Elem().Kind() != reflect.Struct {
		panic("target is not a pointer to a struct")
	}
	val = val.Elem()
	valtype := val.Type()

	for i := 0; i < valtype.NumField(); i++ {
		field := valtype.Field(i)
		if field.PkgPath != "" {
			continue
		}

		tag, ok := field.Tag.Lookup("flag")
		if !ok {
			continue
		}
		tagparts := strings.SplitN(tag, ",", 2)
		if len(tagparts) != 2 {
			continue
		}
		name := tagparts[0]
		usage := tagparts[1]

		fieldval := val.Field(i)
		switch ptr := fieldval.Addr().Interface().(type) {
		case *bool:
			f.BoolVar(ptr, name, *ptr, usage)
		case *time.Duration:
			f.DurationVar(ptr, name, *ptr, usage)
		case *float64:
			f.Float64Var(ptr, name, *ptr, usage)
		case *int:
			f.IntVar(ptr, name, *ptr, usage)
		case *int64:
			f.Int64Var(ptr, name, *ptr, usage)
		case *string:
			f.StringVar(ptr, name, *ptr, usage)
		case *uint:
			f.UintVar(ptr, name, *ptr, usage)
		case *uint64:
			f.Uint64Var(ptr, name, *ptr, usage)
		default:
			fv, ok := fieldval.Interface().(flag.Value)
			if !ok {
				panic("unknown field type " + fieldval.Type().String())
			}
			f.Var(fv, name, usage)
		}
	}

}

// SetFromCommandline sets the flags from the structags on the
// flags.CommandLine Flagset
// identical to SetFromFlagSet(flag.CommandLine, target)
func SetFromCommandline(target interface{}) {
	SetFromFlagSet(flag.CommandLine, target)
}
