package flagutil

import (
	"flag"
	"testing"
)

func Test_Flags(t *testing.T) {

	test := &struct {
		A int    `flag:"a,testflag a"`
		B string `flag:"b,testflag b"`
		C string `flag:"c,defaulted c"`
	}{A: 10, B: "test", C: "defaultVal"}

	f := flag.NewFlagSet("testprog", flag.PanicOnError)

	SetFromFlagSet(f, test)
	f.Parse([]string{"-a", "42", "-b", "test"})
	if test.A != 42 {
		t.Errorf("A != 42: %v", test.A)
	}
	if test.B != "test" {
		t.Errorf("B != \"test\": %v", test.B)
	}
	if test.C != "defaultVal" {
		t.Errorf("B != \"defaultVal\": %v", test.C)
	}
}
